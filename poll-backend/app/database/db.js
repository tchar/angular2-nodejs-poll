var mongoose   = require('mongoose');
mongoose.Promise = require('bluebird');

var url = 'test-shard-00-00-asy9t.mongodb.net:27017,test-shard-00-01-asy9t.mongodb.net:27017,test-shard-00-02-asy9t.mongodb.net:27017/poll?ssl=true&replicaSet=Test-shard-0&authSource=admin';
var username = 'root'
var password = 'kms72hJHZg9kR2pt'
var uri = 'mongodb://' + username + ':' + password + '@' + url;

var MAX_CONNECT_ATTEMPTS = 10;
var connectAttempts = 0;

function connect(){
	if (connectAttempts > MAX_CONNECT_ATTEMPTS){
		return;
	}
	mongoose.connect(uri, { useMongoClient: true });
	connectAttempts++;
}

mongoose.connection.on('connected', function(){
	console.log('Connected to db url:', url);
})

mongoose.connection.on('disconnected', function() {
	console.log('Disconnected from db url:', url)
	console.log('Attempting to reconnect');
	connect();
})

mongoose.connection.on('error', (error) => {
	console.log('Connection error to db:', url);
	console.log('Error:', error);
})

exports.connect = connect;