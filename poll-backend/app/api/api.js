const express 		= require('express');
const pollCtrl 		= require('../controllers/poll_controller')
const router = express.Router();

router.get('/poll/:id', pollCtrl.getPoll);
router.post('/poll', pollCtrl.postPoll);
router.post('/poll/:id/vote', pollCtrl.postVote)
module.exports = router;