var mongoose 		= require('mongoose');
var Schema = mongoose.Schema;

var ChoiceSchema = new Schema({
	text: String,
	votes: Number
})

var PollSchema = new Schema({
	question: String,
	choices: [ChoiceSchema],
	type: Number
});

PollSchema.virtual('TYPE_SINGLE').get(function () {
    return 0;
});

PollSchema.virtual('TYPE_MULTI').get(function () {
    return 1;
});

PollSchema.methods.getPollTypeText = function(){
    if (this.type == 1){
    	return 'Multiple Choice';
    } else{
    	return 'Single Choice';
    }
}

PollSchema.pre('save', function(next){
	if (this.isNew){
		var poll = this;
		poll.question = poll.question.trim();
		if (poll.question.length == 0 || poll.choices.length < 2){
			var err = new Error('Question cannot be empty');
			next(err);
		}

		if (poll.type != poll.TYPE_SINGLE && poll.type != poll.TYPE_MULTI){
			var err = new Error('Poll type must be defined');
			next(err);
		}

		var choicesLen = poll.choices.length;
		for (var i = choicesLen - 1; i >= 0; i--){
			poll.choices[i],text = poll.choices[i].text.trim();
			if (poll.choices[i].length == 0){
				poll.choices.splice(i, 1);
			}
		}
		if (poll.choices.length < 2){
			var err = new Error('You must provide at least two options');
			next(err);
		}
	}
	next();
})

var Poll = mongoose.model('Poll', PollSchema);

module.exports = Poll;