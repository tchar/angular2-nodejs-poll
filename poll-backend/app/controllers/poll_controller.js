const Poll      = require('../models/poll');

exports.getPoll = function(req, res){
	var promise = Poll.findById(req.params.id)
	promise.then(function(doc){
		res.json(doc);
	})
	.catch(function(err){
		res.status(404).send();
	})
}

exports.postPoll = function(req, res){
	var poll = new Poll();
	poll.question = req.body.question;
	req.body.choices.forEach((item, index) => {
		poll.choices.push({text: item.text, votes: 0});
	});
	poll.type = req.body.type;

	var promise = poll.save();
	promise.then(function(doc) {
		res.json(doc);
	}).catch(function(err){
		console.log(err);
		res.status(400).send();
	})
}

function validVote(newPoll, oldPoll){
	var valid = true;
	var votesChanged = 0;
	oldPoll.choices.forEach((item, index) => {
		if (!valid) {
			return;
		}
		if (item.text != newPoll.choices[index].text){
			valid = false;
		}
		var diff = newPoll.choices[index].votes - item.votes;
		if (diff < 0 || diff > 1){
			valid = false;
		}
		if (diff == 1){
			votesChanged++;
		}
	})
	if (oldPoll.type == oldPoll.TYPE_MULTI){
		return valid && (votesChanged > 0);

	} else if (oldPoll.type == oldPoll.TYPE_SINGLE) {
		return valid && (votesChanged == 1);
	}
}

exports.postVote = function(req, res){
	var prom = Poll.findById(req.params.id)
	var newPoll = req.body;
	prom.then(function(doc){
		if (!(validVote(newPoll, doc))){
			res.status(400).send();
		} else {
			doc.choices.forEach((item, index) => {
				item.votes = newPoll.choices[index].votes;
			});
			var promSave = doc.save();
			promSave.then((doc)=>{
				res.json(doc);
			}).catch((err) => {
				res.status(500).send();
			})
		}
	}).catch(function(err){
		console.log(err);
		res.status(404).send();
	})
}