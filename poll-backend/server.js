var express 		= require('express');
var app 			= express();
var bodyParser		= require('body-parser');
var db 				= require('./app/database/db.js');
var path 			= require('path');
var api 			= require('./app/api/api')
var cors 			= require('cors')

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

db.connect();

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist')));

// Set our api routes
app.use('/api', api);

// Catch all other routes and return the index file
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});


var port = process.env.PORT || 8080;

app.listen(port);
console.log('Listening...');