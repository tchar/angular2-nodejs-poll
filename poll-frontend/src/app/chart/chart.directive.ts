import { Directive, Input, HostListener, OnInit } from '@angular/core';
import { Chart } from '../results/chart'
import {Poll} from '../poll/poll'

declare var google:any;

@Directive({
  selector: '[chart]'
})
export class ChartDirective implements OnInit {
  
	private static googleLoaded:any;

	@Input() chart: Chart;

	@HostListener('window:resize') onResize() {
		this.drawChart();
	}

  private chartWrapper: any;

	constructor() {
	}

	ngOnInit() {
		if(!ChartDirective.googleLoaded) {
			ChartDirective.googleLoaded = true;
			google.charts.load('current',  {packages: ['corechart']});
		}
		google.charts.setOnLoadCallback(() => this.createChart());
	}

	createChart() {
      	this.chartWrapper = new google.visualization.ChartWrapper({
	        chartType: this.chart.getType(),
	        dataTable: google.visualization.arrayToDataTable(this.chart.getDataTable()),
	        options:this.chart.getOptions(),
	        containerId: "chart"
      });

      	this.drawChart();
	}

	drawChart(){
		this.chartWrapper.draw();
	}

}
