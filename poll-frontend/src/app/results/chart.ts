import { Colors } from "./colors"
import { Poll } from '../poll/poll'

export class Chart {

	private dataTable: any[];
	private options: any;
	private type: String; 

	constructor (poll: Poll, type: String, options: any = null) {
		this.type = type;

		this.options = {
			legend:"none",
			height: "100%",
			width: "100%",
	        backgroundColor:"transparent",
	        // titleTextStyle: { color: "#777" },
	        colors: Colors.getColors(poll.choices.length),
	        pieSliceText: "label",
	        chartArea: {
	            height: "94%",
	            width: "94%"
	        }
		}

		if (options) {
			this.options = options;
		}

		this.dataTable = [['SimplePoll', 'Votes']]
		poll.choices.forEach((item) => {
			this.dataTable.push([item.text, item.votes])
		});
	}

	getDataTable(): any[] {
		return this.dataTable;
	}

	getOptions(): any {
		return this.options;
	}

	getType(): String {
		return this.type;
	}
}

export class PieChart extends Chart {

	constructor(poll: Poll, options: any = null){
		super(poll, "PieChart", options);
	}

}