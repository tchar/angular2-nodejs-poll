import { Component, OnInit, OnDestroy } from '@angular/core';
import { Http} from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router'
import 'rxjs/add/operator/toPromise';
import {PieChart} from './chart'
import { environment } from '../../environments/environment'
import { Poll, Choice } from '../poll/poll'

@Component({
	selector: 'app-results',
	templateUrl: './results.component.html',
	styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit, OnDestroy {

	private sub: any;
	poll: Poll;
    chart: PieChart;
    votesSum: number = null;

    public pieChartLabels:string[] = ['Download Sales', 'In-Store Sales', 'Mail Sales'];
  	public pieChartData:number[] = [300, 500, 100];
  	public pieChartType:string = 'pie';
  	
	constructor(private http: Http, 
				private route: ActivatedRoute,
				private router: Router) { }

	calcPercentage(choice: Choice): String {
		if (this.votesSum == null){
			this.votesSum = this.poll.choices.reduce((a, b) => a + b.votes, 0);
		}
		var ret = (choice.votes / this.votesSum * 100);
		if (isNaN(ret)){
			return "0%";
		} else {
			return ret.toFixed(1) + "%";
		}
	}

	pluralize(choice: Choice): String {
		if (choice.votes != 1){
			return "votes";
		} else {
			return "vote";
		}
	}

	getColor(i): String {
		return this.chart.getOptions().colors[i];
	}

	ngOnInit() {
		this.sub = this.route.params.subscribe(params => {
     		this.http.get(environment.baseUrl + '/api/poll/' + params['id'])
     			.toPromise()
     			.then((res) => {
     				var poll: Poll = Poll.fromJson(res.json());
     				poll.choices.sort((a, b) => { return b.votes - a.votes});
     				this.chart = new PieChart(poll);
     				this.poll = poll;
     			})
     			.catch((error ) => {
					console.log(error);
     				this.router.navigate(['404']);
     			});
    	});
	}

	ngOnDestroy() {
		this.sub.unsubscribe();
		this.poll = null;
		this.chart = null;
	}

}
