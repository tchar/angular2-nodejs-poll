
export class Colors {

	private static colorLuminance(hex, lum) {
	    // validate hex string
	    hex = String(hex).replace(/[^0-9a-f]/gi, '');
	    if (hex.length < 6) {
	        hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
	    }
	    lum = lum || 0;

	    // convert to decimal and change luminosity
	    var rgb = "#", c, i;
	    for (i = 0; i < 3; i++) {
	        c = parseInt(hex.substr(i*2,2), 16);
	        c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
	        rgb += ("00"+c).substr(c.length);
	    }
	    return rgb;
	}

	private static generateColorsList(listNum) {
		var arr = [];
	    var colorPool = ['#69c', '#c69', '#99d', '#d99', '#c96', '#96c'];
	    var step = 0.2;
	    for (var i = 0; i < listNum; i+=2){
	        var newColor = colorPool[Math.floor(i/2) % colorPool.length];
	        arr[i] = newColor;
	        arr[i+1] = Colors.colorLuminance(newColor, step);
	    }
	    return arr;
	}

	static getColors(n: number){
		return Colors.generateColorsList(n);
	}

	constructor(){}
}