import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { PollComponent } from './poll/poll.component';
import { ResultsComponent } from './results/results.component';
import { PopupsComponent } from './popups/popups.component';
import { VoteComponent } from './vote/vote.component';
import { NotFoundComponent } from './not-found/not-found.component';

import { ChartDirective } from './chart/chart.directive';

// Define the routes
const ROUTES = [
	{path: '', redirectTo: 'poll', pathMatch: 'full'},
	{path: 'poll/:id/results', component: ResultsComponent},
	{path: 'poll/:id', component: VoteComponent},
	{path: 'poll', component: PollComponent},
	{ path: '404', component: NotFoundComponent },
	{ path: '**', redirectTo: '404' }
];


@NgModule({
	declarations: [
		AppComponent,
		PollComponent,
		ResultsComponent,
		PopupsComponent,
		VoteComponent,
		NotFoundComponent,
		ChartDirective,
	],
	imports: [
		BrowserModule,
		FormsModule,
		HttpModule,
		RouterModule.forRoot(ROUTES),
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
