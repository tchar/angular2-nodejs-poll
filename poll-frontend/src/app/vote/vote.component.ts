import { Component, OnInit, OnDestroy } from '@angular/core';
import { Http} from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router'
import 'rxjs/add/operator/toPromise';
import {Popup} from '../popups/popup'
import { environment } from '../../environments/environment';
import {Poll} from '../poll/poll'

@Component({
	selector: 'app-vote',
	templateUrl: './vote.component.html',
	styleUrls: ['./vote.component.scss']
})
export class VoteComponent implements OnInit, OnDestroy {
	poll: Poll;
	checkChoices: Boolean[];
	radioChoices: String;
	popup: Popup =  new Popup();
	private sub: any;

	private setPoll(poll){
		this.checkChoices = [];
		for (let choice in poll.choices){
			this.checkChoices.push(false);
		}
		this.radioChoices = null;
		this.poll = Poll.fromJson(poll);
	}

	submitVote() {
		if (!this.poll.validVote(this.radioChoices, this.checkChoices)){
			this.popup.setMsg('Please select at least one option');
			this.popup.setError(true);
			this.popup.show();
			return;
		}

		this.http.post(environment.baseUrl + '/api/poll/' + this.poll.id + '/vote', this.poll.toVoteJson(this.radioChoices, this.checkChoices))
			.toPromise()
			.then((res) => {
 				this.router.navigate(['poll', String(this.poll.id), 'results']);
			})
			.catch((error) => {
				console.log(error);
				this.popup.setMsg('Something went wrong please try again');
				this.popup.setError(true);
				this.popup.show();
			})
	}
	constructor(private http: Http, 
				private route: ActivatedRoute,
				private router: Router) { }

	ngOnInit() {
		this.sub = this.route.params.subscribe(params => {
     		this.http.get(environment.baseUrl + '/api/poll/' + params['id'])
     			.toPromise()
     			.then((res) => {
     				this.setPoll(res.json());
     			})
     			.catch((error ) => {
     				this.router.navigate(['404']);
     			});
    	});
	}

	ngOnDestroy(){
		this.sub.unsubscribe();
		this.poll = null;
		this.checkChoices = null;
		this.radioChoices = null;
	}

}
