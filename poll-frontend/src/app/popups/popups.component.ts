import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import {Popup} from './popup'

@Component({
	selector: 'app-popups',
	templateUrl: './popups.component.html',
	styleUrls: ['./popups.component.scss']
})
export class PopupsComponent implements OnInit {

	@Input() popup: Popup;

	constructor(router: Router) { }

	hidePopup() {
		this.popup.hide();
		this.popup.setError(false);
		this.popup.setMsg('');
	}

	// redirectToPoll() {
	// 	console.log(this.popup.getUrl());
	// 	this.router.navigate(this.popup.getUrl());
	// }

	ngOnInit() {
	}

}
