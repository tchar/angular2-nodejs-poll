export class Popup {

	private display: boolean = false;
	private url: string[] = [];
	private error: boolean = false;
	private newPoll: boolean = false;
	private msg: string = '';
	private baseUrl: string = '';

	getMsg() : string {
		return this.msg
	}

	setMsg(msg: string) {
		this.msg = msg;
	}

	isError(): boolean {
		return this.error
	}

	setError(error: boolean) {
		this.error = error;
	}

	setBaseUrl(url: string){
		this.baseUrl = url;
	}

	setUrl(url: string[]){
		this.url = url;
	}

	setNewPoll(newPoll: boolean){
		this.newPoll = newPoll;
	}

	getNewPoll() : boolean{
		return this.newPoll;
	}

	getUrl() : string[] {
		var url = this.url.map(x => x);
		if (url && url.length != 0 && url[0][0] != '/'){
			url.unshift('/');
		}
		return url;
	}

	getUrlStringWithBase(): string {
		var url = this.url.map(x => x);
		url.unshift(this.baseUrl);
		return url.join('/');
	}

	getUrlString(): string {
		return this.url.join('/');
	}

	getDisplay(): boolean {
		return this.display;
	}

	hide() {
		this.display = false;
		this.url = [];
		this.msg = '';
		this.error = false;
		this.newPoll = false;
		this.baseUrl = '';
	}

	show() {
		this.display = true;
	}

	constructor(){
	}
}