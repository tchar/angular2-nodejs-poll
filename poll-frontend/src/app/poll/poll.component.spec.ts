import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PollComponent } from './poll.component';
import { FormsModule } from '@angular/forms';
import { PopupsComponent } from '../popups/popups.component';
import { HttpModule } from '@angular/http';
 import { RouterTestingModule } from '@angular/router/testing';

describe('PollComponent', () => {
  let component: PollComponent;
  let fixture: ComponentFixture<PollComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PollComponent, PopupsComponent ],
      imports: [
        HttpModule,
        FormsModule,
        RouterTestingModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
