import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Popup } from '../popups/popup'
import {PlatformLocation } from '@angular/common';
import {Poll, Choice} from './poll'
import { environment } from '../../environments/environment';


@Component({
	selector: 'app-poll',
	templateUrl: './poll.component.html',
	styleUrls: ['./poll.component.scss']
})
export class PollComponent implements OnInit {
	poll: Poll = new Poll();

	popup: Popup = new Popup();

	choiceChanged(index) {
		if (index == this.poll.choices.length - 1){
        	this.poll.choices.push(new Choice('', 0));
        }
	}

	newPoll() {
		if (!this.poll.validQuestion()){
			this.popup.setMsg('Please provide a question');
			this.popup.setError(true);
			this.popup.show();
			return
		}

		if (!this.poll.validChoices()){
			this.popup.setMsg('Please provide at least two options');
			this.popup.setError(true);
			this.popup.show();
			return;
		}

		this.http.post(environment.baseUrl + '/api/poll', this.poll.toJson())
			.toPromise()
			.then((res) => {
				this.popup.setMsg('Your poll has been created, click the link below to redirect to your poll.')
				this.popup.setBaseUrl((this.location as any).location.origin);
				this.popup.setUrl(['poll', String(res.json()._id)]);
				this.popup.setNewPoll(true);
				this.popup.show();
			})
			.catch((error) => {
				console.log(error);
				this.popup.setMsg('Something went wrong please try again');
				this.popup.setError(true);
				this.popup.show();
			})

	}
	constructor(private location: PlatformLocation, private http: Http, private router: Router) { }

	ngOnInit() {
	}

}
