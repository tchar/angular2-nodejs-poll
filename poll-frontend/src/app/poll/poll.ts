export class Choice {
	text: string = null;
	votes: number = 0;

	static fromJson(choice: any): Choice{
		return new Choice(choice.text, choice.votes);
	}

	toJson(): any {
		if (this.text != null && this.text.trim() != ''){
			return {
				text: this.text,
				votes: this.votes
			}
		} else {
			return null;
		}
	}

	constructor(text: string, votes: number){
		this.text = text;
		this.votes = votes;
	}
}

export class Poll {

	question: String = ''
	choices: Choice[] = [new Choice('', 0), new Choice('', 0)]
	type: number = 0;
	id: String = ''

	toJson(): any{
		var newPoll = {
			question: this.question,
			choices: [], 
			type: this.type
		};
		for (let choice of this.choices){
			var choiceJson = choice.toJson();
			if (choiceJson){
				newPoll.choices.push(choiceJson);
			}
		}
		return newPoll;
	}

	validQuestion() : Boolean {
		return this.question.trim() != ''
	}

	validChoices(): Boolean {
		var valids = 0;
		for(let choice of this.choices){
			if (choice.text.trim() != ''){
				valids++;
			}
			if (valids > 1){
				break;
			}
		}
		return valids > 1;
	}

	validVote(radio: String, check: Boolean[]): Boolean {
		if (this.type == 0){
			if (radio && !isNaN(+radio) && +radio >= 0 && +radio < this.choices.length){
				return true;
			} else {
				return false;
			}
		} else if (this.type == 1){
			var valid = check.some((item) => {
				if (item == true){
					return true;
				} else {
					return false;
				}
			});
			return valid;
		}
		return false;
	}

	toVoteJson(radio: String, check: Boolean[]): any{
		if (this.type == 0){
			this.choices[+radio].votes++;
		} else if (this.type == 1){
			var choices = [];
			check.forEach((item, index) => {
				if (item==true){
					this.choices[index].votes++;
				}
			});
		}
		return this.toJson();
	}

	static fromJson(poll: any): Poll {
		var ret = new Poll();
		ret.question = poll.question;
		ret.choices = [];
		for (var choice of poll.choices){
			ret.choices.push(Choice.fromJson(choice));
		}
		ret.type = poll.type;
		ret.id = poll._id;
		return ret;
	}

	getPollTypeText(){
		if (this.type == 0){
			return 'Single Choice'
		} else {
			return 'Multi Choice'
		}
	}

	constructor(){

	}
}