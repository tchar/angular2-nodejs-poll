import { CanActivate }    from '@angular/router';


export class Guard implements CanActivate {

  canActivate():  Promise<boolean> {
    return new Promise((resolve) => {
          resolve(true);
    });
  }
}